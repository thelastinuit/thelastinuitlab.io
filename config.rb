activate :blog do |blog|
  blog.name = "resume"
  blog.prefix = "resume"
  blog.permalink = "{year}{month}{day}{title}.html"
  blog.sources = "{year}-{month}-{day}-{title}.html"
  blog.taglink = "{tag}.html"
  blog.layout = "resume_layout"
end

activate :blog do |blog|
  blog.name = "home"
  blog.permalink = "{year}{month}{day}{title}.html"
  blog.sources = "{year}-{month}-{day}-{title}.html"
  blog.taglink = "{tag}.html"
  blog.layout = "layout"
end

configure :build do
  activate :minify_css
  activate :minify_javascript
end
